<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Spatie\Url\Url;

class UploadController extends Controller
{
    //

    public function uploadFile(Request $request): string
    {
        $file = $request->file('file');
        $fext = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
        $fnam = md5(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . microtime());
        $fsiz = number_format($file->getSize() / 1024, 2, ".", "");
        $fful = $fnam . "." . $fext;

        $modeledFile = File::create([
            'file_name' => $fnam,
            'file_type' => $fext,
            'file_size' => $fsiz,
        ]);
        
        $modeledFile->save();

        Storage::disk('local')->put('public/files/' . $fful, $file->get());
        
        // constructs a url
        $url = Url::fromString($request->getSchemeAndHttpHost());
        return $url->getScheme() . '//' . env('UPLOADER_SUBDOMAIN') . $url->getHost() . '/' . $fful;
    }

    public function queryFile(Request $request): JsonResponse 
    {
        $request->validate([
            'file_name' => ['required', 'string'],
        ]);

        $file = File::where('file_name', $request->query('file_name'))->firstOrFail();   
    
        return response()->json([
            'file_name' => $file->file_name,
            'file_type' => $file->file_type,
            'file_size' => $file->file_size,

            'created_at' => $file->created_at,
        ]);
    }
}

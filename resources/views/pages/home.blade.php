@extends('layouts.app')

@section('content') 
    <div class="px-4">
        <component-container>
            <h1 class="text-2xl text-center text-black dark:text-white">You are logged in!</h1>
        </component-container>
    </div>
    
    @section('navtab')
    <navbar-tab href={{ url('/tokens') }}>
        Personal Access Tokens
    </navbar-tab>
    @endsection
@endsection
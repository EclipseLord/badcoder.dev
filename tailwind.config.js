module.exports = {
	content: [
		'./resources/**/*.vue',
		'./storage/framework/views/*.php',
		'./resources/**/*.blade.php',
		'./resources/**/*.js',
	],
	theme: {
		extend: {	
			fontFamily: { 
				sans: 'Nunito',
			},

			colors: {
				'prim-dark': {
					100: '#383838',
					200: '#2c2c2c',
					300: '#181818',
				},

				'prim-nshades': { 
					100: '#4aa264',
					200: '#5f704a',
					300: '#7f6042',
					400: '#a14e38',
					500: '#c33e34',
				},
			},
		},
	},
	variants: {
		extend: {},
	},
	plugins: [],
}

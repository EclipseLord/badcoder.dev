const mix = require('laravel-mix');
require('mix-tailwindcss');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.ts('resources/js/app.ts', 'public/js')
    .vue({ version: 3 })
    .postCss('resources/css/app.css', 'public/css')
    .tailwind('./tailwind.config.js')

if(mix.inProduction()) {
    mix.version();
    mix.webpackConfig({
    });
} else {
    require('laravel-mix-eslint')
    mix.webpackConfig({
        devtool: 'eval-source-map',
    });
    mix.eslint({
        fix: false,
        extensions: ['js', 'ts', 'vue'],
    });
}

// shut up now!
mix.disableNotifications();
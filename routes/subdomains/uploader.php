<?php

use Symfony\Component\HttpFoundation\BinaryFileResponse;

Route::get('/{file_id}', function(Request $request, $file_id): BinaryFileResponse {
    return response()->file('storage/files/' . $file_id);
});